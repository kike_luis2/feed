package com.example.unix.feed;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;


public class HandleXML {

    //Declaracion de variables
    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingComplete = true;

    //Array de Titulos
    ArrayList<String> arrayList = new ArrayList<String>();

    //Array de Descripcion
    ArrayList<String> arrayListDes = new ArrayList<String>();

    //Array de Link
    ArrayList<String> arrayListLink = new ArrayList<>();

    //Constructor para obtener la url rss
    public HandleXML(String url){
        this.urlString = url;
    }

   //Contructor de array de titulos
    public ArrayList<String> getArrayList() {
        return arrayList;
    }

    //Constructor de array de descripcion
    public ArrayList<String> getArrayListDes() {
        return arrayListDes;
    }

    //Constructor de array de link
    public ArrayList<String> getArrayListLink(){
        return arrayListLink;
    }

    //Metodo para agregar titulo y descripcion al array
    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;

        try {
            event = myParser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT) {
                String name=myParser.getName();

                switch (event){
                    case XmlPullParser.START_TAG:
                        break;

                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;

                    case XmlPullParser.END_TAG:

                        if(name.equals("title")){

                            arrayList.add(text);
                        }

                        else if(name.equals("description")){

                            arrayListDes.add(text);
                        }

                        else if(name.equals("link")){

                            arrayListLink.add(text);
                        }

                        else{
                        }

                        break;
                }
                event = myParser.next();
            }

            parsingComplete = false;
        }

        catch (Exception e) {
            e.printStackTrace();
        }
        //aqui
    }

    //Metodo para la conexion al rss
    public void fetchXML(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {

                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);

                    // Inica la conexion
                    conn.connect();
                    InputStream stream = conn.getInputStream();

                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();

                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);

                    //Ejecuta el metodo para agregar datos al array
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                }

                catch (Exception e) {
                }
            }
        });

        //Ejecuto el hilo
        thread.start();
    }
}
