package com.example.unix.feed;

import android.app.Activity;

import java.util.ArrayList;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ListView;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

public class MainActivity extends Activity implements NavigationDrawerFragment.NavigationDrawerCallbacks {


    private NavigationDrawerFragment mNavigationDrawerFragment;

    private CharSequence mTitle;

    EditText title,link,description;
    private String finalUrl="http://feeds.feedburner.com/cnnmexico/deportes?format=xml";
    private HandleXML obj;
    public static String nombre;
    private ImageView imageView;
    private Bitmap loadedImage;

    private String imageHttpAddress = "http://jonsegador.com/wp-content/apezz.png";

    public static ArrayList<String> arrayList = new ArrayList<String>();
    public static ArrayList<String> arrayListDes = new ArrayList<>();
    public static ArrayList<String> arrayListLink = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {

       super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        //Obtiene el titulo de la aplicacion
        mTitle = getTitle();

        // Inicializa el menu lateral
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        //Creacion de objeto
        obj = new HandleXML(finalUrl);

        //Ejecuta el metodo de conexion al rss
        obj.fetchXML();

        while(obj.parsingComplete);

        arrayList = obj.getArrayList();
        arrayListDes = obj.getArrayListDes();
        arrayListLink = obj.getArrayListLink();
    }



    @Override
    public void onNavigationDrawerItemSelected(int position) {

        // cambia o reemplaza el contenido de la principal
       FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();

    }




    public void onSectionAttached(int number) {
        switch (number) {

            case 1:
                mTitle = getString(R.string.title_section1);
                break;

        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {

            //aqui muestra el contenido seleccionado

            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            //Creamos un nuevo ArrayAdapter con nuestra lista de cosasPorHacer
            ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(),
                    android.R.layout.simple_list_item_1, arrayList);

            //Seleccionamos la lista de nuestro layout
            ListView miLista = (ListView) rootView.findViewById(R.id.listView);

            //Se agregan los titulos al listview
            miLista.setAdapter(arrayAdapter);

            //Evento al seleccionar un elemento del listview
            miLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    String datos = "datos";
                    String link = "link";

                    for (int x = 0; x < arrayListDes.size(); x++) {
                        if (position == x) {
                            datos = arrayListDes.get(x);


                            for (int y = 0; y < arrayListLink.size(); y++) {

                                if (position == y) {
                                    link = arrayListLink.get(y);
                                    break;
                                }

                            }

                        }
                    }

                    Intent i = new Intent(getActivity(), com.example.unix.feed.noticias.class);
                    i.putExtra("datos", datos);
                    i.putExtra("link", link);
                    startActivity(i);
                }
            });

            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

            if(getArguments().getInt(ARG_SECTION_NUMBER) == 2) {

                new AlertDialog.Builder(getActivity())
                        .setTitle("Acerca de")
                        .setMessage("Version 1 Actualizacion 0, para obtener mas informacion sobre esta app, visite www.acercade.mx")
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setNegativeButton(android.R.string.no, null).show();
            }

        }
    }

}
