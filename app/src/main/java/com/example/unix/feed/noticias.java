package com.example.unix.feed;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;


public class noticias extends Activity {

    private NavigationDrawerFragment mNavigationDrawerFragment;

    private WebView webView;
    private ImageView imageView;
    private Bitmap loadedImage;
    private String imageHttpAddress = "http://jonsegador.com/wp-content/apezz.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_noticias);

        Bundle bundle = this.getIntent().getExtras();

        String datos = bundle.getString("datos");
        String imageHttpAddress = bundle.getString("link");
        //imageView = (ImageView) findViewById(R.id.imageView);

       // downloadFile(imageHttpAddress);

        System.out.println("-------------------------> :" +imageHttpAddress);
        webView = (WebView)this.findViewById(R.id.webView);

        webView.loadData(datos, "text/html; charset=utf-8","utf-8");

        webView.setWebViewClient(new WebViewClient() {
            // evita que los enlaces se abran fuera nuestra app en el navegador de android
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

        });

    }

    void downloadFile(String imageHttpAddress) {
        URL imageUrl = null;
        try {
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            loadedImage = BitmapFactory.decodeStream(conn.getInputStream());
            imageView.setImageBitmap(loadedImage);
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Error cargando la imagen: "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            //aqui muestra el contenido seleccionado
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
    }






}
